<!DOCTYPE HTML>
<html>
    <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <link src="" rel="stylesheet">
        <title>Galactivity</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, height=device-height">
        <meta name="description" content="Find something to do today, right now.">
        <meta name="keywords" content="Activities, Events, Free time">
        <meta name="author" content="Galactivity LLC">
        <script src="js/app.js"></script>
        <style>
            html,body {
                width: 100%;
                height: 100%;
                margin: 0;
                padding: 0px;
                overflow-x: hidden;
            }
            *{
                box-sizing: border-box;
            }
            #navbar {
                z-index: 1031;
                width: 100vw;
            }
            .v-header{
                height: 100vh;
                display: flex;
                align-items: center;
                color: #fff;
            }
            .container {
                max-width: 960px !important;
                padding-left: 1rem !important;
                padding-right: 1rem !important;
                margin: auto !important;
                text-align: center !important;
                z-index: 0 !important;
            }
            .container2 {
                max-width: 1920px !important;
                padding-left: 0 !important;
                padding-right: 0 !important;
                margin: auto !important;
                text-align: center !important;
                z-index: 2 !important;
                width: 100vw !important;
            }
            .fullscreen-video-wrap{
                position:absolute;
                top:0;
                left: 0;
                width: 100%;
                height: 100vh;
                overflow: hidden;
            }
            .fullscreen-video-wrap video{
                min-width: 100%;
                min-height: 100%;
            }
            .ts30 {
                font-size: 60px;
            }
            .header-overlay {
                height: 100vh;
                width: 100vw;
                position:absolute;
                top:0;
                left:0;
                background: gray;
                z-index: 1;
                opacity: 0.50;
            }
            .header-content{
                margin: auto;
                z-index:2;
            }
            @media(max-width:960px) {
                .container {
                    padding-right: 3rem;
                    padding-left: 3rem;
                }
            }
            .bg-lightersecondary {
                background-color: #989fa6!important;
            }
            .opacity-grd  {
                height: 75px;
                background-image: linear-gradient(to top, rgba(0,0,0,0), 25%,rgba(41, 43, 44, 1));
            }
            .activityBtn{
                transition: all .2s ease-in-out;
            }
            .activityBtn:hover{
                transform: scale(1.1);
            }
        </style>
        <!-- @include('googleinc') -->
    </head>
    <body>
    <div class="container-fluid h-100 d-flex flex-column px-0">
            <nav id="navbar" class="navbar navbar-default navbar-light text-light opacity-grd navbar-expand-md">
                <a class="navbar-brand" href="#">
                    <img src="https://galactivity.com/storage/sitecontent/altlogoINVERSEKOMPRESSED.png" height="50" alt="Galactivity">
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                        <span class="navbar-toggler-icon"></span>
                </button>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav mr-auto">
                            <li class="nav-item">
                                <a class="nav-link text-light" href="https://galactivity.com/activities">Activities</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link text-light" href="https://galactivity.com/pricing">Business Pricing</a>
                            </li>
                        </ul>
                        <ul class="navbar-nav ml-auto">
                                <!-- @guest -->
                                    <li class="nav-item">
                                        <a class="nav-link text-light" href="login">Login</a>
                                    </li>
                                    <!-- @if (Route::has('register'))
                                    <li class="nav-item">
                                        <a class="nav-link ml-2 px-4 text-light btn btn-primary rounded-pill" href="{{ route('register') }}">Register</a>
                                    </li> -->
                                    <!-- @endif -->
                                <!-- @else
                                    <li class="nav-item">
                                        <a class="nav-link ml-auto text-light" href="/userlanding">Dashboard</a>
                                    </li>
                                @endguest -->
                            </li>
                        </ul>
                    </div>
            </nav>
            <header class="v-header container">
                <div class="fullscreen-video-wrap">
                    <video src="https://galactivity.com/storage/webbkg.m4v" autoplay="true" loop="true"></video>
                </div>
                <div class="header-overlay"></div>
                <div class="header-content">
                    <h1 class="text-light font-weight-bold ts30">Discover. Go. Share.</h1>
                    <div class="btn-group py-3" role="group">
                        <a href="https://galactivity.com/activities" class="btn btn-primary btn-lg activityBtn">View Activities</a>
                    </div>
                    <div class="container-fluid mt-5 d-none d-md-block"><small class="px-3 py-1 text-center bg-lightersecondary rounded-pill border border-primary text-white text-center">Business? See our pricing for business accounts and their features <a href="https://galactivity.com/pricing" class="text-primary">here</a>.</small></div>
                </div>
            </header>
        </div>
    </body>
</html>
