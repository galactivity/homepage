<?php
  class Activity {
    public $imageSrc;
    public $title;
    public $type;
    public $location;
    public $description;
    function __construct($imageSrc, $title = '', $type = '', $location = '', $description = '') {
      $this->imageSrc = $imageSrc;
      $this->title = $title;
      $this->type = $type;
      $this->location = $location;
      $this->description = $description;
    }
  }
  $featuredActivities = array(
    new Activity('/featured1.jpg'),
    new Activity('/featured2.jpg'),
    new Activity('/featured3.jpg')
  );
  $popularActivities = array(
    new Activity('/popular1.jpg'),
    new Activity('/popular2.jpg'),
    new Activity('/popular3.jpg'),
    new Activity('/popular4.jpg'),
    new Activity('/popular5.jpg'),
    new Activity('/popular6.jpg'),
    new Activity('/popular7.jpg'),
    new Activity('/popular8.jpg')
  );
  $familyFriendly = array(
    new Activity('/popular1.jpg'),
    new Activity('/popular2.jpg'),
    new Activity('/popular3.jpg'),
    new Activity('/popular4.jpg'),
  );
?>

<!DOCTYPE HTML>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Galactivity</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, height=device-height">
    <meta name="description" content="Find something to do today, right now.">
    <meta name="keywords" content="Activities, Events, Free time">
    <meta name="author" content="Galactivity LLC">
    <style>
      @font-face {
        font-family: "Montserrat";
        src: url(/montserrat-regular.ttf) format("truetype");
        font-weight: normal;
      }
      @font-face {
        font-family: "Montserrat";
        src: url(/montserrat-bold.ttf) format("truetype");
        font-weight: bold;
      }
      html,body {
        width: 100%;
        height: 100%;
        margin: 0px;
        padding: 0px;
        overflow-x: hidden;
        font-family: Montserrat;
        background-color: #231F20;
      }
    </style>
  </head>
  <body>
    <div id="app">
      <header-el authed="false"></header-el>
      <activities-page></activities-page>
      <div style="height: 100px"></div>
      <featured-section
        activities-json=<?php echo json_encode($featuredActivities, JSON_HEX_TAG) ?>
      ></featured-section>
      <div style="display: flex; justify-content: center; left: 0px; right: 0px; margin-bottom: 188px">
        <div style="display: flex; flex-direction: column; align-items: center; max-width: 1200px">
          <div style="align-self: flex-start; font-size: 20px; font-weight: bold; line-height: 28px; color: white">
            Make plans
            <span style="font-size: 14px; margin-left: 6px; font-weight: normal">this weekend</span>
          </div>
          <div style="width: 100%; display: flex; justify-content: space-between">
            <activity-cell-large
              image-src="<?php echo $familyFriendly[1]->imageSrc?>"
              title="<?php echo $familyFriendly[1]->title?>"
              type="<?php echo $familyFriendly[1]->type?>"
              location="<?php echo $familyFriendly[1]->location?>"
            >
            </activity-cell-large>
            <div style="flex: 1; max-width: 20px">
            </div>
            <activity-cell-large
              image-src="<?php echo $familyFriendly[2]->imageSrc?>"
              title="<?php echo $familyFriendly[2]->title?>"
              type="<?php echo $familyFriendly[2]->type?>"
              location="<?php echo $familyFriendly[2]->location?>"
            >
            </activity-cell-large>
            <div style="flex: 1; max-width: 20px">
            </div>
            <activity-cell-large
              image-src="<?php echo $familyFriendly[3]->imageSrc?>"
              title="<?php echo $familyFriendly[3]->title?>"
              type="<?php echo $familyFriendly[3]->type?>"
              location="<?php echo $familyFriendly[3]->location?>"
            >
            </activity-cell-large>
          </div>
        </div>
      </div>
      <activity-page-section
        title="Family friendly"
        activities-json=<?php echo json_encode($familyFriendly, JSON_HEX_TAG) ?>
      >
      </activity-page-section>
      <activity-page-section
        title="Fun with friends"
        style="background: transparent"
        activities-json=<?php echo json_encode($familyFriendly, JSON_HEX_TAG) ?>
      >
      </activity-page-section>
      <activity-page-section
        title="Hidden gems"
        activities-json=<?php echo json_encode($familyFriendly, JSON_HEX_TAG) ?>
      >
      </activity-page-section>
      <activity-page-section
        title="Bucket list"
        style="background: transparent"
        activities-json=<?php echo json_encode($familyFriendly, JSON_HEX_TAG) ?>
      >
      </activity-page-section>
      <bottom-spacer></bottom-spacer>
      <footer-el></footer-el>
    </div>
    <script src="js/app.js"></script>
  </body>
</html>
