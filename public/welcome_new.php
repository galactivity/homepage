<?php
  class Activity {
    public $imageSrc;
    public $title;
    public $type;
    public $location;
    public $description;
    function __construct($imageSrc, $title = '', $type = '', $location = '', $description = '') {
      $this->imageSrc = $imageSrc;
      $this->title = $title;
      $this->type = $type;
      $this->location = $location;
      $this->description = $description;
    }
  }
  $featuredActivities = array(
    new Activity('/featured1.jpg'),
    new Activity('/featured2.jpg'),
    new Activity('/featured3.jpg')
  );
  $popularActivities = array(
    new Activity('/popular1.jpg'),
    new Activity('/popular2.jpg'),
    new Activity('/popular3.jpg'),
    new Activity('/popular4.jpg'),
    new Activity('/popular5.jpg'),
    new Activity('/popular6.jpg'),
    new Activity('/popular7.jpg'),
    new Activity('/popular8.jpg')
  );
?>
<!DOCTYPE HTML>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Galactivity</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, height=device-height">
    <meta name="description" content="Find something to do today, right now.">
    <meta name="keywords" content="Activities, Events, Free time">
    <meta name="author" content="Galactivity LLC">
    <style>
      @font-face {
        font-family: "Montserrat";
        src: url(/montserrat-regular.ttf) format("truetype");
        font-weight: normal;
      }
      @font-face {
        font-family: "Montserrat";
        src: url(/montserrat-bold.ttf) format("truetype");
        font-weight: bold;
      }
      html,body {
        width: 100%;
        height: 100%;
        margin: 0px;
        padding: 0px;
        overflow-x: hidden;
        font-family: Montserrat;
        background-color: #231F20;
      }
    </style>
  </head>
  <body>
    <div id="app">
      <header-el authed="false"></header-el>
      <title-section></title-section>
      <featured-section
        activities-json=<?php echo json_encode($featuredActivities, JSON_HEX_TAG) ?>
      ></featured-section>
      <popular-section
        activities-json=<?php echo json_encode($popularActivities, JSON_HEX_TAG) ?>
      ></popular-section>
      <nearby-section></nearby-section>
      <business-section></business-section>
      <bottom-spacer></bottom-spacer>
      <footer-el></footer-el>
    </div>
    <script src="js/app.js"></script>
  </body>
</html>
