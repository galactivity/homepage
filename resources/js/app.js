/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
const Vuex = require('vuex');
const throttle = require('lodash.throttle');

// Add a store for tracking if the browser window is small enough to be
// considered mobile
const store = new Vuex.Store({
  state: {
    mobile: false,
  },
  mutations: {
    updateMobileState(state) {
      state.mobile = window.innerWidth <= 800;
    }
  }
})

window.addEventListener(
  'resize',
  throttle(() => store.commit('updateMobileState'), 200)
);
store.commit('updateMobileState');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('activity-cell', require('./components/ActivityCell.vue').default);
Vue.component('header-link', require('./components/HeaderLink.vue').default);
Vue.component('header-image', require('./components/HeaderImage.vue').default);
Vue.component('footer-link', require('./components/FooterLink.vue').default);
Vue.component('featured-slider', require('./components/FeaturedSlider.vue').default);
Vue.component('featured-cell', require('./components/FeaturedCell.vue').default);
Vue.component('square-button', require('./components/SquareButton.vue').default);
Vue.component('round-button', require('./components/RoundButton.vue').default);
Vue.component('featured-section', require('./components/FeaturedSection.vue').default);
Vue.component('popular-section', require('./components/PopularSection.vue').default);
Vue.component('nearby-section', require('./components/NearbySection.vue').default);
Vue.component('business-section', require('./components/BusinessSection.vue').default);
Vue.component('bottom-spacer', require('./components/BottomSpacer.vue').default);
Vue.component('footer-el', require('./components/Footer.vue').default);
Vue.component('header-el', require('./components/Header.vue').default);
Vue.component('title-section', require('./components/TitleSection.vue').default);
Vue.component('heart-button', require('./components/HeartButton.vue').default);
Vue.component('popup', require('./components/Popup.vue').default);
Vue.component('activity-sign-up-modal', require('./components/ActivitySignUpModal.vue').default);
Vue.component('sign-up-modal', require('./components/SignUpModal.vue').default);
Vue.component('sign-in-modal', require('./components/SignInModal.vue').default);
Vue.component('auth-modal', require('./components/AuthModal.vue').default);
Vue.component('sign-up-email-modal', require('./components/SignUpEmailModal.vue').default);
Vue.component('entry-field', require('./components/EntryField.vue').default);
Vue.component('forgot-password-modal', require('./components/ForgotPasswordModal.vue').default);
Vue.component('activities-page', require('./components/ActivitiesPage.vue').default);
Vue.component('button-selector', require('./components/ButtonSelector.vue').default);
Vue.component('drop-selector', require('./components/DropSelector.vue').default);
Vue.component('drop-selector-option', require('./components/DropSelectorOption.vue').default);
Vue.component('activity-page-section', require('./components/ActivityPageSection.vue').default);
Vue.component('activity-cell-large', require('./components/ActivityCellLarge.vue').default);
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    store,
});
