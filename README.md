## Galactivity Homepage

The updated homepage design wrapped in a Laravel project. Includes Vue components and a php page.

See the following files/directories:
- public/welcome_new.php
- resources/js/components
